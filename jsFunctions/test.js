const x= '{maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:white_wool", Count: 18b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 2, uses: 7, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}';

const y = 'white_wool\n' +
'orange_wool\n' +
'magenta_wool\n' +
'light_blue_wool\n' +
'yellow_wool\n' +
'lime_wool\n' +
'pink_wool\n' +
'gray_wool\n' +
'light_gray_wool\n' +
'cyan_wool\n' +
'purple_wool\n' +
'blue_wool\n' +
'brown_wool\n' +
'green_wool\n' +
'red_wool\n' +
'black_wool';

const wools = y.split('\n');

const xs = wools.map(one=>{
  return x.replace(wools[0], one);
})

console.log(xs.join(','));
