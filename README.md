# FrogMCS_Villager_Code

Creation command codes of villagers.
For any modification and additional command, please submit PR and notify admins in chat channel.

---
command save:

1. get a op sword:
```
/give @p minecraft:diamond_sword{display:{Name:"[{\"text\":\"杀村民专用\",\"bold\":false,\"italic\":false,\"underlined\":false,\"strikethrough\":false,\"obfuscated\":false}]"},Enchantments:[{id:"minecraft:sharpness",lvl:999}]} 1
```

2. get a real villager's NBT:
```
/data get entity @e[limit=1, sort=nearest, type=minecraft:villager] {}
```

3. 没有幻翼的时候召唤幻翼：
```
execute unless entity @e[type=minecraft:phantom] run summon minecraft:phantom ~ ~20 ~
```
or（在最近的玩家头顶20召唤）
```
execute at @p unless entity @e[type=minecraft:phantom] run summon minecraft:phantom ~ ~20 ~
```

4. 获取玩家头
```
/give @p minecraft:player_head{SkullOwner:Notch}
```

5. 命令方块上面的箱子里第一格变成64个沙砾
```
/replaceitem block ~ ~1 ~ container.0 minecraft:gravel 64
```

6. 召唤一个装满沙子和沙砾的骡子
```
/summon minecraft:mule ~2 ~1 ~ {CustomName:"[{\"text\":\"圣诞老骡\",\"color\":\"gray\",\"bold\":true,\"italic\":true,\"underlined\":false,\"strikethrough\":false,\"obfuscated\":false}]",Glowing:1b,ActiveEffects:[{Id:28,Amplifier:0,Duration:30}],ChestedHorse:1b,Tame:1b,Health:1,Attributes:[{Name:"generic.maxHealth",Base:1}],
Items:[
{id:"minecraft:gravel",Count:64b,Slot:0b},
{id:"minecraft:sand",Count:64b,Slot:1b},
{id:"minecraft:gravel",Count:64b,Slot:2b},
{id:"minecraft:sand",Count:64b,Slot:3b},
{id:"minecraft:gravel",Count:64b,Slot:4b},
{id:"minecraft:gravel",Count:64b,Slot:5b},
{id:"minecraft:sand",Count:64b,Slot:6b},
{id:"minecraft:gravel",Count:64b,Slot:7b},
{id:"minecraft:sand",Count:64b,Slot:8b},
{id:"minecraft:gravel",Count:64b,Slot:9b},
{id:"minecraft:gravel",Count:64b,Slot:10b},
{id:"minecraft:sand",Count:64b,Slot:11b},
{id:"minecraft:gravel",Count:64b,Slot:12b},
{id:"minecraft:sand",Count:64b,Slot:13b},
{id:"minecraft:gravel",Count:64b,Slot:14b}]}
```

7. 找到最近的某号地图的展示框
```
/data get entity @e[type=minecraft:item_frame, limit=1, sort=nearest, nbt={Item: {id: "minecraft:filled_map", tag: {map: 236}}}]
```
干掉附近的某号地图展示框
```
/kill @e[type=minecraft:item_frame, limit=1, sort=nearest, nbt={Item: {id: "minecraft:filled_map", tag: {map: 236}}}]
```
干掉所有的某号地图展示框
```
/kill @e[type=minecraft:item_frame, nbt={Item: {id: "minecraft:filled_map", tag: {map: 236}}}]
```

8. 每个僵尸给自己脚下插一个火把（与这里距离500内）
```
/execute as @e[type=zombie, distance=..500] at @s run setblock ~ ~ ~ torch keep
```

9. 每个僵尸给自己脚下插一个火把（y为0~100，防止给高处的刷怪塔也插上了）
```
/execute as @e[type=zombie, x=-5000, y=0, z=-5000, dx=10000, dy=100, dz=10000] at @s run setblock ~ ~ ~ torch keep
```

10. 每个溺尸给自己脚下插一个光源（先周围100小范围实验）
```
/execute as @e[type=drowned, distance=..100] at @s if block ~ ~ ~ water run setblock ~ ~ ~ light[level=15, waterlogged=true] replace
```
11. 实体过多警告方块(参考 https://gaming.stackexchange.com/questions/365931/how-to-count-entities-with-commands-check-if-there-are-only-one-or-a-certain-num )

(1)先设计分板
```
/scoreboard objectives add entityCount dummy
```

（2）多个命令方块。第一个设为循环、不受制约、保持开启，第二个设为连锁、条件制约、保持开启。方块相邻摆放，方向要对，第一个指着第二个。
然后第三个用来告警，脉冲、条件制约、红石控制，用一个慢一点的脉冲，防止刷屏。如果想让整体少算一点，把第一个也设成第三个这样来用红石控制也行
```
/scoreboard players set count entityCount 0
/execute as @e run scoreboard players add count entityCount 1
/execute if score count entityCount matches 600.. run say 警告，全服实体超过600个，请注意！随机举例10个实体：@e[limit=10, sort=random]
```

（3）如果想在所有人屏幕上显示分数，用这个命令。(sidebar是右侧,belowName是自己头上,list是按快捷键Tab看到的玩家列表,sidebar.team.<颜色>设置只有某一颜色的队伍能看见)
```
/scoreboard objectives setdisplay sidebar entityCount
```

（4）如果想在单独一些人屏幕上显示分数，要用队伍功能，单独给某个颜色的队伍的人显示分数。好像还能用来设玩家名字颜色？很酷炫的样子。
```
/scoreboard objectives setdisplay sidebar.team.aqua entityCount
/team add 237team
/team join 237team 237th
/team modify 237team color aqua
```

12. 修改传送门（用于资源世界更新后重建西瓜门）

```
#0. 在某个地方设置目的地：用小木斧左键右键选一个区域，然后输入下面命令。资源世界末地返回目的地一般设在末地入口，所以这里叫做res_portal_to_end。
/mvp modify loc -p res_portal_to_end

#1. 删除传送门（如果修改loc的话好像不生效，可能mvp modify有bug，不如删了重建）
/mvp remove res_end_to_res

#2. 用小木斧左右键选择门的对角（在新的末地自己搭一个门，不要靠近塔，靠近的话召龙时会被删掉）
#3. 新建传送门，并且设置目的地
/mvpc res_end_to_res p:res_portal_to_end

#4. 修改门目的地
/mvpmodify dest res_portal_to_end
```
更多mvp指令详情：https://github.com/Multiverse/Multiverse-Core/wiki/Command-Reference-%28Portals%29#create


## 13. 宝可梦救助队

(1)命令方块让离某个队的人最近的人加入队伍，要求距离10以内。好像没啥用，以后再用来做其他玩法吧。

```
/execute as @p[sort=nearest, limit=1, distance=..10] at @p[team=helperTeam] run team join helperTeam
```

（2）命令方块让离救助队最近的人传送，要求距离5以内。利用0.001防止传走自己。

* 如果去掉type=player，能传送任意实体。这个功能可以以后改改用于传送动物。

```
/execute at @p[team=helperTeam] run tp @e[sort=nearest, limit=1, distance=0.001..5, type=player] @e[type=cat, sort=random, limit=1]
```

(3)让救助队周围的生物起飞
```
/execute at @p[team=helperTeam] run tp @e[sort=nearest, limit=1, distance=..5, team=!helperTeam] ~ ~10 ~
```