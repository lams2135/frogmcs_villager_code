/summon minecraft:villager ~ ~0.5 ~ {CustomName:"\"萌新人口贩子\"",
ActiveEffects:[{Id:10,Amplifier:254,Duration:9999999}],
ArmorItems:[{},{},{id:"minecraft:turtle_helmet",Count:1b,tag:{Enchantments:[{id:"minecraft:thorns",lvl:999999},{id:"minecraft:protection",lvl:999999}]}},{}],
ArmorDropChances:[-1f,-1f,-1f,-1f],
VillagerData:{profession:fletcher,level:5,type:swamp},NoAI:1,
Rotation:[90f,0f],
Offers:{Recipes:[
            {
                maxUses:99999999, buy: {id: "bat_spawn_egg", Count: 1}, sell: {
                    id: "minecraft:villager_spawn_egg", Count: 1, tag: {
                        Damage: 1, display: {Name: "\"萌新皮匠\""},
                        EntityTag: {
                            NoAI: 1, HandItems: [{id: "leather_leggings", Count: 1}, {id: "rabbit_hide", Count: 1}], HandDropChances: [0.0, 1.0],
                            VillagerData: {type: "savanna", profession: "leatherworker", lvl: 5}, Offers: {
                            Recipes:[{maxUses:9999999,buyB:{id:"minecraft:air",Count:1b},buy:{id:"minecraft:flint",Count:26b},sell:{id:"minecraft:emerald",Count:1b},xp:10,uses:0,priceMultiplier:0.05f,specialPrice:0,demand:0,rewardExp:1b},{maxUses:9999999,buyB:{id:"minecraft:air",Count:1b},buy:{id:"minecraft:emerald",Count:5b},sell:{id:"minecraft:leather_helmet",Count:1b,tag:{Damage:0,display:{color:12250278}}},xp:5,uses:12,priceMultiplier:0.2f,specialPrice:0,demand:0,rewardExp:1b},{maxUses:9999999,buyB:{id:"minecraft:air",Count:1b},buy:{id:"minecraft:rabbit_hide",Count:9b},sell:{id:"minecraft:emerald",Count:1b},xp:20,uses:7,priceMultiplier:0.05f,specialPrice:0,demand:0,rewardExp:1b},{maxUses:9999999,buyB:{id:"minecraft:air",Count:1b},buy:{id:"minecraft:emerald",Count:7b},sell:{id:"minecraft:leather_chestplate",Count:1b,tag:{Damage:0,display:{color:8439583}}},xp:1,uses:12,priceMultiplier:0.2f,specialPrice:0,demand:0,rewardExp:1b},{maxUses:9999999,buyB:{id:"minecraft:air",Count:1b},buy:{id:"minecraft:scute",Count:4b},sell:{id:"minecraft:emerald",Count:1b},xp:30,uses:0,priceMultiplier:0.05f,specialPrice:0,demand:0,rewardExp:1b},{maxUses:9999999,buyB:{id:"minecraft:air",Count:1b},buy:{id:"minecraft:emerald",Count:6b},sell:{id:"minecraft:leather_horse_armor",Count:1b},xp:15,uses:12,priceMultiplier:0.2f,specialPrice:0,demand:0,rewardExp:1b},{maxUses:9999999,buyB:{id:"minecraft:air",Count:1b},buy:{id:"minecraft:emerald",Count:6b},sell:{id:"minecraft:saddle",Count:1b},xp:30,uses:0,priceMultiplier:0.2f,specialPrice:0,demand:0,rewardExp:1b},{maxUses:9999999,buyB:{id:"minecraft:air",Count:1b},buy:{id:"minecraft:emerald",Count:5b},sell:{id:"minecraft:leather_helmet",Count:1b,tag:{Damage:0,display:{color:5203882}}},xp:30,uses:0,priceMultiplier:0.2f,specialPrice:0,demand:0,rewardExp:1b}]
                            }, 
                            ArmorItems:[{},{},{},{id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"Tree_Ocean"}}],
                            id: "minecraft:villager"
                        }
                    }
                }
            },
            {
                maxUses:99999999, buy: {id: "bat_spawn_egg", Count: 1}, sell: {
                    id: "minecraft:villager_spawn_egg", Count: 1, tag: {
                        Damage: 1, display: {Name: "\"见习助理石匠\""},
                        EntityTag: {
                            NoAI: 1, HandItems: [{id: "stone", Count: 1}, {}], HandDropChances: [1f, 0.0f],
                            VillagerData: {type: "savanna", profession: "mason", lvl: 5}, Offers: {
                                Recipes: [{
                                             maxUses: 9999999,
                                             buyB: { id: "minecraft:air", Count: 1 },
                                             buy: { id: "minecraft:clay_ball", Count: 10 },
                                             sell: { id: "minecraft:emerald", Count: 1 },
                                             rewardExp: 1
                                           }, {
                                             maxUses: 9999999,
                                             buyB: { id: "minecraft:air", Count: 1 },
                                             buy: { id: "minecraft:emerald", Count: 1 },
                                             sell: { id: "minecraft:brick", Count: 10 },
                                             rewardExp: 1
                                           }, {
                                             maxUses: 9999999,
                                             buyB: { id: "minecraft:air", Count: 1 },
                                             buy: { id: "minecraft:stone", Count: 20 },
                                             sell: { id: "minecraft:emerald", Count: 1 },
                                             rewardExp: 1
                                           }, {
                                             maxUses: 9999999,
                                             buyB: { id: "minecraft:air", Count: 1 },
                                             buy: { id: "minecraft:emerald", Count: 1 },
                                             sell: { id: "minecraft:chiseled_stone_bricks", Count: 4 },
                                             rewardExp: 1
                                           }, {
                                             maxUses: 9999999,
                                             buyB: { id: "minecraft:air", Count: 1 },
                                             buy: { id: "minecraft:diorite", Count: 16 },
                                             sell: { id: "minecraft:emerald", Count: 1 },
                                             rewardExp: 1
                                           }, {
                                             maxUses: 9999999,
                                             buyB: { id: "minecraft:air", Count: 1 },
                                             buy: { id: "minecraft:emerald", Count: 1 },
                                             sell: { id: "minecraft:polished_granite", Count: 4 },
                                             rewardExp: 1
                                           }, {
                                             maxUses: 9999999,
                                             buyB: { id: "minecraft:air", Count: 1 },
                                             buy: { id: "minecraft:emerald", Count: 1 },
                                             sell: { id: "minecraft:polished_diorite", Count: 4 },
                                             rewardExp: 1
                                           }, {
                                             maxUses: 9999999,
                                             buyB: { id: "minecraft:air", Count: 1 },
                                             buy: { id: "minecraft:emerald", Count: 1 },
                                             sell: { id: "minecraft:polished_andesite", Count: 4 },
                                             rewardExp: 1
                                           }, {
                                             maxUses: 9999999,
                                             buyB: { id: "minecraft:air", Count: 1 },
                                             buy: { id: "minecraft:emerald", Count: 1 },
                                             sell: { id: "minecraft:terracotta", Count: 1 , tag:{display:{Name:"\"陶瓦，自己染8\""}} },
                                             rewardExp: 1
                                           }, {
                                             maxUses: 9999999,
                                             buyB: { id: "minecraft:air", Count: 1 },
                                             buy: { id: "minecraft:emerald", Count: 1 },
                                             sell: { id: "minecraft:brown_glazed_terracotta", Count: 1},
                                             rewardExp: 1
                                           }, {
                                             maxUses: 9999999,
                                             buyB: { id: "minecraft:air", Count: 1 },
                                             buy: { id: "minecraft:emerald", Count: 1 },
                                             sell: { id: "minecraft:red_glazed_terracotta", Count: 1 },
                                             rewardExp: 1
                                           }, {
                                             maxUses: 9999999,
                                             buyB: { id: "minecraft:air", Count: 1 },
                                             buy: { id: "minecraft:emerald", Count: 1 },
                                             sell: { id: "minecraft:quartz_pillar", Count: 1 },
                                             rewardExp: 1
                                           }, {
                                             maxUses: 9999999,
                                             buyB: { id: "minecraft:air", Count: 1 },
                                             buy: { id: "minecraft:emerald", Count: 1 },
                                             sell: { id: "minecraft:quartz_block", Count: 1 },
                                             rewardExp: 1
                                           }]
                            }, id: "minecraft:villager"
                        }
                    }
                }
            },
            {
                maxUses:99999999, buy: {id: "bat_spawn_egg", Count: 1}, sell: {
                    id: "minecraft:villager_spawn_egg", Count: 1, tag: {
                        Damage: 1, display: {Name: "\"萌新屠夫\""},
                        EntityTag: {
                            NoAI: 1, HandItems: [{id: "porkchop", Count: 1}, {}], HandDropChances: [1f, 0.0f],
                            VillagerData: {type: "savanna", profession: "butcher", lvl: 5}, Offers: {
                                Recipes: [{maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:porkchop", Count: 7b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 2, uses: 0, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:rabbit", Count: 4b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 2, uses: 16, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:coal", Count: 15b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 2, uses: 0, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:emerald", Count: 1b}, sell: {id: "minecraft:cooked_porkchop", Count: 5b}, xp: 5, uses: 16, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:mutton", Count: 7b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 20, uses: 0, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:beef", Count: 10b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 20, uses: 6, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:dried_kelp_block", Count: 10b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 30, uses: 6, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:sweet_berries", Count: 10b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 30, uses: 0, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}]
                            }, id: "minecraft:villager"
                        }
                    }
                }
            },
            {
                maxUses:99999999, buy: {id: "bat_spawn_egg", Count: 1}, sell: {
                    id: "minecraft:villager_spawn_egg", Count: 1, tag: {
                        Damage: 1, display: {Name: "\"萌新制图师\""},
                        EntityTag: {
                            NoAI: 1, HandItems: [{id: "map", Count: 1}, {}], HandDropChances: [1f, 0.0f],
                            VillagerData: {type: "savanna", profession: "cartographer", lvl: 5}, Offers: {
                                Recipes: [{maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:paper", Count: 24b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 2, uses: 0, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:emerald", Count: 7b}, sell: {id: "minecraft:map", Count: 1b}, xp: 1, uses: 10, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:glass_pane", Count: 11b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 10, uses: 10, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:compass", Count: 1b}, buy: {id: "minecraft:emerald", Count: 13b}, sell: {id: "minecraft:filled_map", Count: 1b, tag: {Decorations: [{x: 1136.0d, z: -1312.0d, id: "+", type: 9b, rot: 180.0d}], map: 102, display: {MapColor: 3830373, Name: '{"translate":"filled_map.monument"}'}}}, xp: 5, uses: 0, priceMultiplier: 0.2f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:compass", Count: 1b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 20, uses: 0, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:compass", Count: 1b}, buy: {id: "minecraft:emerald", Count: 14b}, sell: {id: "minecraft:filled_map", Count: 1b, tag: {Decorations: [{x: 23744.0d, z: 18640.0d, id: "+", type: 8b, rot: 180.0d}], map: 103, display: {MapColor: 5393476, Name: '{"translate":"filled_map.mansion"}'}}}, xp: 10, uses: 0, priceMultiplier: 0.2f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:emerald", Count: 3b}, sell: {id: "minecraft:red_banner", Count: 1b}, xp: 15, uses: 5, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:emerald", Count: 3b}, sell: {id: "minecraft:black_banner", Count: 1b}, xp: 15, uses: 0, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:emerald", Count: 8b}, sell: {id: "minecraft:globe_banner_pattern", Count: 1b}, xp: 30, uses: 0, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}]
                            }, id: "minecraft:villager"
                        }
                    }
                }
            },
            {
                maxUses:99999999, buy: {id: "bat_spawn_egg", Count: 1}, sell: {
                    id: "minecraft:villager_spawn_egg", Count: 1, tag: {
                        Damage: 1, display: {Name: "\"萌新牧师\""},
                        EntityTag: {
                            NoAI: 1, HandItems: [{id: "experience_bottle", Count: 1}, {}], HandDropChances: [1f, 0.0f],
                            VillagerData: {type: "savanna", profession: "cleric", lvl: 5}, Offers: {
                                Recipes: [{maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:rotten_flesh", Count: 32b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 2, uses: 0, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:emerald", Count: 1b}, sell: {id: "minecraft:redstone", Count: 2b}, xp: 1, uses: 12, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:gold_ingot", Count: 3b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 10, uses: 0, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:emerald", Count: 1b}, sell: {id: "minecraft:lapis_lazuli", Count: 1b}, xp: 5, uses: 12, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:rabbit_foot", Count: 2b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 20, uses: 0, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:emerald", Count: 4b}, sell: {id: "minecraft:glowstone", Count: 1b}, xp: 10, uses: 12, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:glass_bottle", Count: 9b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 30, uses: 0, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:emerald", Count: 5b}, sell: {id: "minecraft:ender_pearl", Count: 1b}, xp: 15, uses: 12, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:nether_wart", Count: 22b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 30, uses: 0, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:emerald", Count: 3b}, sell: {id: "minecraft:experience_bottle", Count: 1b}, xp: 30, uses: 0, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}]
                            }, id: "minecraft:villager"
                        }
                    }
                }
            },
            {
                maxUses:99999999, buy: {id: "bat_spawn_egg", Count: 1}, sell: {
                    id: "minecraft:villager_spawn_egg", Count: 1, tag: {
                        Damage: 1, display: {Name: "\"毛毛牧羊人\""},
                        EntityTag: {
                            NoAI: 1, HandItems: [{id: "white_wool", Count: 1}, {}], HandDropChances: [1f, 0.0f],
                            VillagerData: {type: "savanna", profession: "leatherworker", lvl: 5}, Offers: {
                                Recipes: [
                                {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:white_wool", Count: 18b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 2, uses: 7, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b},{maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:orange_wool", Count: 18b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 2, uses: 7, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b},{maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:magenta_wool", Count: 18b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 2, uses: 7, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b},{maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:light_blue_wool", Count: 18b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 2, uses: 7, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b},{maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:yellow_wool", Count: 18b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 2, uses: 7, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b},{maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:lime_wool", Count: 18b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 2, uses: 7, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b},{maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:pink_wool", Count: 18b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 2, uses: 7, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b},{maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:gray_wool", Count: 18b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 2, uses: 7, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b},{maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:light_gray_wool", Count: 18b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 2, uses: 7, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b},{maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:cyan_wool", Count: 18b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 2, uses: 7, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b},{maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:purple_wool", Count: 18b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 2, uses: 7, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b},{maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:blue_wool", Count: 18b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 2, uses: 7, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b},{maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:brown_wool", Count: 18b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 2, uses: 7, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b},{maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:green_wool", Count: 18b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 2, uses: 7, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b},{maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:red_wool", Count: 18b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 2, uses: 7, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b},{maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:black_wool", Count: 18b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 2, uses: 7, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b},
                                 {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:white_dye", Count: 12b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 10, uses: 0, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:emerald", Count: 1b}, sell: {id: "minecraft:pink_carpet", Count: 4b}, xp: 5, uses: 16, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:emerald", Count: 3b}, sell: {id: "minecraft:orange_bed", Count: 1b}, xp: 10, uses: 12, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:emerald", Count: 3b}, sell: {id: "minecraft:light_gray_bed", Count: 1b}, xp: 10, uses: 0, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:magenta_dye", Count: 12b}, sell: {id: "minecraft:emerald", Count: 1b}, xp: 30, uses: 0, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:emerald", Count: 3b}, sell: {id: "minecraft:gray_banner", Count: 1b}, xp: 15, uses: 1, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}, {maxUses: 9999999, buyB: {id: "minecraft:air", Count: 1b}, buy: {id: "minecraft:emerald", Count: 2b}, sell: {id: "minecraft:painting", Count: 3b}, xp: 30, uses: 0, priceMultiplier: 0.05f, specialPrice: 0, demand: 0, rewardExp: 1b}]
                            }, id: "minecraft:shepherd"
                        }
                    }
                }
            }
]}}
