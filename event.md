# 一、巨人和召唤物活动

## 1、 召唤巨人
```
execute unless entity @e[tag=the_big_giant] run summon minecraft:giant ~ ~1 ~ {CustomName:"[{\"text\":\"巨人\",\"bold\":false,\"italic\":false,\"underlined\":false,\"strikethrough\":false,\"obfuscated\":false}]",Tags:["the_big_giant"],CustomNameVisible:1b,PersistenceRequired:1b,Health:1024,Invulnerable:0b,Passengers:[{CustomName:"[{\"text\":\"邪恶的艾尔迪亚人\"}]",Tags:["the_giant_passenger"],CustomNameVisible:1b,ArmorItems:[{},{},{},{id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"237th"}}],Attributes:[{Name:"generic.follow_range",Base:100}],Invulnerable:1,id:"minecraft:stray"}],Attributes:[{Name:"generic.maxHealth",Base:1024},{Name:"generic.knockback_resistance",Base:0.9},{Name:"generic.armor",Base:30},{Name:"generic.armor_toughness",Base:20}],ActiveEffects:[{Id:10,Amplifier:10,Duration:199999980,ShowParticles:0b},{Id:12,Amplifier:5,Duration:199999980,ShowParticles:0b}],ArmorItems:[{id:"minecraft:iron_boots",Count:1b},{id:"minecraft:iron_leggings",Count:1b},{id:"minecraft:iron_chestplate",Count:1b,tag:{Enchantments:[{id:"minecraft:thorns",lvl:10},{id:"minecraft:protection",lvl:999999}]}},{id:"minecraft:iron_helmet",Count:1b}]}
```

## 2、用命令方块在巨人附近召唤： 35%概率掉附魔书的双刀玩家头横扫怪。移速很快而且会起飞，很还原。太强了，加个虚弱18、挖掘疲劳4削弱下（好像没效果？）。那把回血效果去掉好了。
```
execute at @e[tag=the_big_giant] unless entity @e[tag=the_swepping_monster] run summon minecraft:stray ~ ~10 ~ {CustomName:"[{\"text\":\"调查兵团\",\"bold\":false,\"italic\":false,\"underlined\":false,\"strikethrough\":false,\"obfuscated\":false}]", Tags:["the_swepping_monster"], CustomNameVisible:1b,Health: 36,Motion: [-0.3d, 2d, -0.2d],Fire: 0,Attributes:[{Name:"generic.movement_speed",Base:0.6},{Name:"generic.maxHealth",Base:36},{Name:"generic.attack_damage",Base:0.0},{Name:"generic.follow_range",Base:100},{Name:"generic.knockback_resistance",Base:0.3}],ActiveEffects:[{Id:10,Amplifier:1,Duration:60},{Id:8,Amplifier:15,Duration:1000} ,{Id:30,Amplifier:3,Duration:199999980},{Id:29,Amplifier:3,Duration:199999980},{Id:24,Amplifier:1,Duration:199999980},{Id:18,Amplifier:20,Duration:6000}, {Id:4,Amplifier:20,Duration:6000}],HandItems:[{id:"minecraft:wooden_sword",Count:1b,tag:{Enchantments:[{id:"minecraft:sweeping",lvl:3},{id:"minecraft:knockback",lvl:2}]}},{id:"minecraft:wooden_sword",Count:1b,tag:{Enchantments:[{id:"minecraft:sweeping",lvl:3},{id:"minecraft:knockback",lvl:2}]}}],ArmorItems:[{id:"minecraft:leather_boots",Count:1b,tag:{Enchantments:[{id:"minecraft:feather_falling",lvl:4}]}},{id:"minecraft:leather_leggings",Count:1b},{id:"minecraft:enchanted_book",Count:1b,tag:{StoredEnchantments:[{id:"minecraft:sweeping",lvl:3}]}},{id:"minecraft:player_head",Count:1b,tag:{SkullOwner:{Id:[I;-1210264445,-389396553,-1119126311,-1057213962],Properties:{textures:[{Value:"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMmRhNWJlM2I3MDVmMzFiNDUxYTQ2MTExMGQ0YTkwMGVjN2MwZGZiMmZlMjI5NmFmYWY0YmMyNjNiNGRiNDI2NSJ9fX0="}]}}}}],HandDropChances:[0.5f,0.5f],ArmorDropChances:[0.1f,0.8f,0.035f,0.05f]}
```


附1、（单独召唤横扫怪的命令）
```
/summon minecraft:stray ~ ~1 ~ {CustomName:"[{\"text\":\"调查兵团\",\"bold\":false,\"italic\":false,\"underlined\":false,\"strikethrough\":false,\"obfuscated\":false}]", Tags:["the_swepping_monster"], CustomNameVisible:1b,Attributes:[{Name:"generic.movementSpeed",Base:1000}],HandItems:[{id:"minecraft:diamond_sword",Count:1b,tag:{Enchantments:[{id:"minecraft:sweeping",lvl:3}]}},{id:"minecraft:diamond_sword",Count:1b,tag:{Enchantments:[{id:"minecraft:sweeping",lvl:3}]}}],ArmorItems:[{},{},{id:"minecraft:enchanted_book",Count:1b,tag:{StoredEnchantments:[{id:"minecraft:sweeping",lvl:3}]}},{id:"minecraft:player_head",Count:1b,tag:{SkullOwner:{Id:[I;-1210264445,-389396553,-1119126311,-1057213962],Properties:{textures:[{Value:"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMmRhNWJlM2I3MDVmMzFiNDUxYTQ2MTExMGQ0YTkwMGVjN2MwZGZiMmZlMjI5NmFmYWY0YmMyNjNiNGRiNDI2NSJ9fX0="}]}}}}],HandDropChances:[-1f,-1f],ArmorDropChances:[0.0f,0.0f,0.5f,0.0f]}
```

附2、（拿兵长玩家头）
```
/give @p minecraft:player_head{display:{Name:"{\"text\":\"Levi Ackerman\"}"},SkullOwner:{Id:[I;-1210264445,-389396553,-1119126311,-1057213962],Properties:{textures:[{Value:"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMmRhNWJlM2I3MDVmMzFiNDUxYTQ2MTExMGQ0YTkwMGVjN2MwZGZiMmZlMjI5NmFmYWY0YmMyNjNiNGRiNDI2NSJ9fX0="}]}}} 1
```

附3、tp到巨人旁边
```
tp @e[tag=the_big_giant]
```

tp到横扫怪旁边
```
tp @e[tag=the_swepping_monster]
```

付4、找到召唤横扫怪的命令方块
```
tp 1824 58 -139
```



# 二、召唤幻翼，没打死不会召唤第二只
```
execute at @p unless entity @e[type=minecraft:phantom] run summon minecraft:phantom ~ ~20 ~
```

## 头活动：用命令方块在附近召唤： 50%概率掉头的双刀玩家头横扫怪
```
execute at @p[limit=1, sort=nearest, distance=..100] unless entity @e[tag=the_kokokio_monster] run summon minecraft:stray ~ ~1 ~ {CustomName:"[{\"text\":\"大叔影舞者\",\"bold\":false,\"italic\":false,\"underlined\":false,\"strikethrough\":false,\"obfuscated\":false}]", Tags:["the_kokokio_monster"], CustomNameVisible:1b,Attributes:[{Name:"generic.movementSpeed",Base:1000}],HandItems:[{id:"minecraft:diamond_sword",Count:1b,tag:{Enchantments:[{id:"minecraft:sweeping",lvl:3}]}},{id:"minecraft:diamond_sword",Count:1b,tag:{Enchantments:[{id:"minecraft:sweeping",lvl:3}]}}],ArmorItems:[{},{},{id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"KOKOKIO"}},{id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"KOKOKIO"}}],HandDropChances:[-1f,-1f],ArmorDropChances:[0.0f,0.0f,0.5f,0.0f]}
```

## 头活动2：用命令方块召唤：掉各种头的怪。好像也不是很肉，几刀就死了。以后加强下
人员： Himamiya 237th AsielQ ProSRLee YibaczZ minatoaqua
```
execute at @p[limit=1, sort=nearest, distance=..100] unless entity @e[tag=the_head_monster_1] run summon minecraft:stray ~ ~1 ~ {CustomName:"[{\"text\":\"头猎人\",\"bold\":false,\"italic\":false,\"underlined\":false,\"strikethrough\":false,\"obfuscated\":false}]", Tags:["the_head_monster_1"], CustomNameVisible:1b,Health:20,Attributes:[{Name:"generic.maxHealth",Base:20},{Name:"generic.knockbackResistance",Base:1000000},{Name:"generic.armor",Base:30},{Name:"generic.armor_toughness",Base:20}],HandItems:[{id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"Himamiya",Enchantments:[{id:"minecraft:sharpness",lvl:5}]}},{id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"237th",Enchantments:[{id:"minecraft:sharpness",lvl:5}]}}],ArmorItems:[{id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"AsielQ"}},{id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"ProSRLee"}},{id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"YibaczZ"}},{id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"minatoaqua"}}],HandDropChances:[0.01f,0.01f],ArmorDropChances:[0.05f,0.05f,0.05f,0.01f],ActiveEffects:[{Id:10,Amplifier:1,Duration:199999980,ShowParticles:1},{Id:12,Amplifier:5,Duration:199999980,ShowParticles:1}]}
```

## 头活动3：碉头怪
人员： _Maqq_ oliviamc1021 Noland_CN XI_YANGEGE kikikiranya Syopac
_Maqq_改名成Maqq_了
```
execute at @p[limit=1, sort=nearest, distance=..100] unless entity @e[tag=the_head_monster_2] run summon minecraft:vindicator ~10 ~10 ~10   {CustomName:"[{\"text\":\"猎头强盗\",\"bold\":false,\"italic\":false,\"underlined\":false,\"strikethrough\":false,\"obfuscated\":false}]", IsBaby: true, Tags:["the_head_monster_2"], CustomNameVisible:1b,Health:50,Attributes:[{Name:"generic.max_health",Base:50},{Name:"generic.attack_damage",Base:6},{Name:"generic.knockback_resistance",Base:0.8},{Name:"generic.armor",Base:5},{Name:"generic.armor_toughness",Base:5},{Name:"generic.movement_speed",Base:0.4},{Name:"zombie.spawn_reinforcements",Base:0.1}],HandItems:[{id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"_Maqq_"}},{id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"oliviamc1021"}}],ArmorItems:[{id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"Syopac"}},{id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"Noland_CN"}},{id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"kikikiranya"}},{id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"XI_YANGEGE"}}],HandDropChances:[0.05f,0.05f],ArmorDropChances:[0.05f,0.05f,0.05f,0.05f],ActiveEffects:[{Id:10,Amplifier:1,Duration:199999980,ShowParticles:0b},{Id:12,Amplifier:2,Duration:199999980},{Id:28,Amplifier:1,Duration:199999980},{Id:8,Amplifier:5,Duration:199999980},{Id:30,Amplifier:5,Duration:199999980},{Id:29,Amplifier:5,Duration:199999980}]}
```

## 头活动4: 射箭头怪，把人射飞。抗性很高，试试能不能让武器和空手打它都一样痛。好像不行，抗性太高空手就只打掉0.2血了，还是弄正常数值。
人员5个： RU1NER 7Bradley OldDove1993 Masaga08 sasa2434
```
execute at @p[limit=1, sort=random, distance=..30] unless entity @e[tag=the_head_monster_3] run summon minecraft:stray ~1 ~3 ~1   {CustomName:"[{\"text\":\"猎头弓箭手\"}]", IsScreamingGoat: true, Tags:["the_head_monster_3"], Motion: [-0.1d, 1.2d, -0.1d],CustomNameVisible:1b,Health:30,Attributes:[{Name:"generic.max_health",Base:30},{Name:"generic.attack_damage",Base:1},{Name:"generic.knockback_resistance",Base:0.8},{Name:"generic.armor",Base:12},{Name:"generic.armor_toughness",Base:8},{Name:"generic.movement_speed",Base:0.52},{Name: "generic.follow_range", Base: 60}],HandItems:[{id:"minecraft:bow", Count: 1,tag:{Enchantments:[{id:"minecraft:punch",lvl:15}]}},{id:"minecraft:player_head", Count: 1,tag:{SkullOwner:"sasa2434"}}],ArmorItems:[{id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"Masaga08"}},{id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"OldDove1993"}},{id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"7Bradley"}},{id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"RU1NER"}}],HandDropChances:[0.0f,0.1f],ArmorDropChances:[0.1f,0.1f,0.1f,0.1f],ActiveEffects:[{Id:12,Amplifier:5,Duration:199999980},{Id:slow_falling,Amplifier:1,Duration:199999980},{Id:8,Amplifier:1,Duration:199999980},{Id:30,Amplifier:1,Duration:199999980},{Id:29,Amplifier:1,Duration:199999980}]}
```

## 头活动5： 骑着幻翼的女巫，轰炸机。属性没改，就是普通的怪
人员6个：魔方 炸鸡 精灵球 灵梦 汉堡 魔理沙
```
execute at @p[limit=1, sort=random, distance=..30] unless entity @e[tag=the_head_monster_5] run summon minecraft:phantom ~1 ~15 ~1 {CustomName:"[{\"text\":\"颚之巨人\"}]",Size: 0, Tags:["the_head_monster_5_horse"],CustomNameVisible:1b,PersistenceRequired:1b,
ActiveEffects:[{Id:12,Amplifier:1,Duration:199999980}],
Passengers:[{CustomName:"[{\"text\":\"猎头骑士\"}]",Tags:["the_head_monster_5"],CustomNameVisible:1b,ArmorItems:[{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"[{\"text\":\"魔方\"}]"},SkullOwner:"ZiGmUnDo"}},{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"[{\"text\":\"炸鸡\"}]"},SkullOwner:"StarfirePrime"}},{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"[{\"text\":\"精灵球\"}]"},SkullOwner:"Magearna"}},{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"[{\"text\":\"灵梦头\"}]"},SkullOwner:{Id:[I;-1812190106,1521241068,-2039543489,806766723],Properties:{textures:[{Value:"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYzJhZTEyYjIxYjJjNjRmNDY0ZGViOGE5Y2FkOWRkODEyMzQyMDkyMmYxYmFiNTUyZWRlYzRhYjZlMDhiNDJjYSJ9fX0="}]}}}}],HandItems:[{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"[{\"text\":\"汉堡\"}]"},SkullOwner:"foodar"}},{id:"minecraft:player_head", Count: 1,tag:{display:{Name:"[{\"text\":\"魔理沙头\"}]"},SkullOwner:{Id:[I;2089801276,-1772993456,-1411839591,-847462349],Properties:{textures:[{Value:"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNTFmZjY1OTQ0NGUwZTdjYmRkYTZkY2JmYTUzYjAyYWQ4YzAxNTZjNmQzYzc4ZWU2MmE4ZTI4YzlkZGUifX19"}]}}}}],HandDropChances:[0.2f,0.2f],ArmorDropChances:[0.2f,0.2f,0.2f,0.2f],Attributes:[{Name:"generic.follow_range",Base:50}],id:"minecraft:witch"}]}
```

## 头活动6 - 骑着僵尸mea的溺尸三叉戟阿夸。不会掉三叉戟。mea跑得快，并且有跳得高效果8 6级。阿夸跑得慢（溺尸跳跃提升没用，僵尸才行）好像偶尔会没有仇恨，a一下就有了。
报名人员7个： yamuyamuyumeno MoChangAn HeavenGrey Androiduck TkingCat mihao Type_White
1.（骑着水生生物的怪，进入水里后会从怪上掉下来，不懂为啥）
2. （骑着大部分怪的溺尸好像不会射三叉戟，怪）

```
execute at @p[limit=1, sort=nearest, distance=..15] unless entity @e[tag=the_head_monster_6] run summon minecraft:zombie ~1 ~2 ~1 {CustomName:"[{\"text\":\"阿夸骑士\"}]", Tags:["the_head_monster_6_horse"],
Size: 5,
CustomNameVisible:1b,PersistenceRequired:1b,Motion: [0.1d, 2d, 0.2d],
ActiveEffects:[{Id:30,Amplifier:2,Duration:199999980},{Id:10,Amplifier:1,Duration:60},{Id:30,Amplifier:3,Duration:199999980},{Id:29,Amplifier:3,Duration:199999980},{Id:24,Amplifier:1,Duration:199999980},{Id:18,Amplifier:20,Duration:6000}, {Id:4,Amplifier:20,Duration:6000},{Id:12,Amplifier:20,Duration:199999980},{Id:8,Amplifier:6,Duration:199999980}],
Attributes:[{Name:"generic.movement_speed",Base:0.30},{Name:"generic.attack_damage",Base:0.1}],
ArmorItems:[{id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"Type_White"}},{id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"MoChangAn"}},{id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"mihao"}},{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"[{\"text\":\"咩啊头\"}]"},SkullOwner:"TanakaShoji"}}],
HandItems:[{id:"stick",Count:1},{id:"apple",Count:1}],ArmorDropChances:[0.2f,0.2f,0.2f,0.2f],HandDropChances:[-1f,0.2f],
IsBaby: true,
Passengers: [{
CustomName:"[{\"text\":\"可爱阿夸\"}]", Tags:["the_head_monster_6"],
CustomNameVisible:1b,PersistenceRequired:1b,
ActiveEffects:[{Id:30,Amplifier:2,Duration:199999980},{Id:10,Amplifier:1,Duration:60},{Id:8,Amplifier:6,Duration:199999980} ,{Id:30,Amplifier:3,Duration:199999980},{Id:29,Amplifier:3,Duration:199999980},{Id:24,Amplifier:1,Duration:199999980},{Id:18,Amplifier:20,Duration:6000}, {Id:4,Amplifier:20,Duration:6000}],
Attributes:[{Name:"generic.movement_speed",Base:0.1}, {Name:"generic.attack_damage",Base:0.1}],
IsBaby: true, Size: 5, ArmorItems:[{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"[{\"text\":\"yamu头\"}]"},SkullOwner:"yamuyamuyumeno"}},{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"[{\"text\":\"HeavenGrey头\"}]"},SkullOwner:"HeavenGrey"}},{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"[{\"text\":\"Androiduck头\"}]"},SkullOwner:"Androiduck"}},{id:"minecraft:player_head",Count:1b,tag:{display:{Name:"[{\"text\":\"他马猫头\"}]"},SkullOwner:"TkingCat"}}],HandItems:[{id:"minecraft:trident", Count: 1,tag:{display:{Name:"[{\"text\":\"阿夸三叉戟\"}]"},SkullOwner:"TanakaShoji"}},{id:"apple",Count:1}],HandDropChances:[-1f,0.2f],ArmorDropChances:[0.2f,0.2f,0.2f,0.2f],
id: "drowned"}]}

```

## 头活动7 - 20230804 - 三重弩烟花头怪

还在研究中，头也还没放进去，还是上面复制的以前的头。
先射一箭漂浮箭，然后射烟花的强盗，骑着强盗。而且跑得快跳得高。用爆炸保护保护强盗自己，不然会被烟花炸死。
问题：怪的发光和跳高效果成功，但是缓降和上升效果好像没有效果；
其他用途：本来想让末影人carriedBlockState拿着一个头，但是这样拿着的好像不能设头名字。而且这次也不是末影人了，以后再试试。

ArmorItems脚部、腿部、胸部和头部
效果28缓降,25漂浮,8跳跃提升jump_boost,24发光，
弩里面放超过3个预装填也只会有3个，而且放别的道具也只能射出普通箭，放药箭比较好。

头：乘客：Memo_RrY Flora0926 (CanpiDaDa),两手拿东西穿盔甲，所以只有3个
马：sq831quq bookonetwo Tree_Ocean (Maqq_) oriannalulu nonesazen
二号乘客：xiaoyu hmj233 CheckerCat Suadade_4049 L_Hamster Enityon

Kuro_Tuki_nn和hmj233好像是原版皮，Kuro_Tuki_nn没放进去

```
execute at @e[limit=1, sort=random, distance=..15] unless entity @e[tag=the_head_monster_7, distance=..100] run summon minecraft:pillager ~1 ~5 ~1 {CustomName:"[{\"text\":\"打上花火\"}]",Size: 0, Tags:["the_head_monster_7_horse"],CustomNameVisible:1b,PersistenceRequired:1b,
Motion: [-0.3d, 2d, -0.2d],
ActiveEffects:[{Id:24,Amplifier:1,Duration:199999980}, {id: 28, Amplifier: 1, Duration: 199999980}, {id: 25, Amplifier: 1, Duration: 10000}, {Id:8,Amplifier:10,Duration:10000}],
Attributes:[{Name:"generic.movement_speed",Base:0.6}],
carriedBlockState:{Name:"minecraft:jack_o_lantern"},
ArmorItems:[
    {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"sq831quq"}},
    {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"bookonetwo"}},
    {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"Tree_Ocean"}},
    {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"Maqq_"}}
    ],
HandItems:[
    {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"oriannalulu"}},
    {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"nonesazen"}},
],
HandDropChances:[0.2f,0.2f],ArmorDropChances:[0.2f,0.2f,0.2f,0.2f],
Passengers:[{CustomName:"[{\"text\":\"肮脏的烟花啊\"}]",Tags:["the_head_monster_7"],CustomNameVisible:1b,
ArmorItems:[
    {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"Memo_RrY"}},
    {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"Flora0926"}},
    {id:"minecraft:chainmail_chestplate",Count:1b,tag:{Enchantments:[{id:"minecraft:blast_protection",lvl:99},{id: "unbreaking", lvl: 99}]}},
    {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"CanpiDaDa"}}
    ],
HandItems:[{id:"minecraft:crossbow",Count:1b,tag:{Enchantments:[{id:"minecraft:quick_charge",lvl:3}, {id:"multishot",lvl:1}, {id: "unbreaking", lvl:3}], Charged:1b, 
ChargedProjectiles: [
    {Count: 1, id: 'tipped_arrow', tag: {CustomPotionEffects: [{Id: 25, Amplifier: 1, Duration: 250}, {Id: 28, Amplifier: 1, Duration: 400}]}},
    {Count: 1, id: 'tipped_arrow', tag: {CustomPotionEffects: [{Id: 25, Amplifier: 1, Duration: 300}, {Id: 28, Amplifier: 1, Duration: 450}]}},
    {Count: 1, id: 'tipped_arrow', tag: {CustomPotionEffects: [{Id: 25, Amplifier: 1, Duration: 350}, {Id: 28, Amplifier: 1, Duration: 500}]}}
    ]}},
{id:"firework_rocket", Count:64b, tag:{Fireworks:{Flight:3, Explosions:[
    {Trail:1b,Flicker:1b,Type:1,Colors:[I;16723968],FadeColors:[I;14744834]},
    {Trail:1b,Flicker:1b,Type:2,Colors:[I;14602026],FadeColors:[I;15435844]},
    {Trail:1b,Flicker:1b,Type:4,Colors:[I;6719955],FadeColors:[I;8073150]}
    ]}} }],
HandDropChances:[0.01f,0.01f],ArmorDropChances:[0.2f,0.2f,0f,0.2f],
Attributes:[{Name:"generic.follow_range",Base:100},{Name:"generic.max_health",Base:50},{Name:"generic.movement_speed",Base:1}],
Health: 50,
ActiveEffects:[{Id:24,Amplifier:1,Duration:199999980},{Id:8,Amplifier:5,Duration:10000},{id: 28, Amplifier: 1, Duration: 10000}],
id:"minecraft:pillager"},
{
CustomName:"[{\"text\":\"啪的一声\"}]",Tags:["the_head_monster_7_horse"],ActiveEffects:[{Id:24,Amplifier:1,Duration:199999980}],
ArmorItems:[
    {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"xiaoyu"}},
    {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"hmj233"}},
    {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"Enityon"}},
    {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"CheckerCat"}}
    ],
HandItems:[
    {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"Suadade_4049"}},
    {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"L_Hamster"}},
],
HandDropChances:[0.2f,0.2f],ArmorDropChances:[0.2f,0.2f,0.2f,0.2f],
IsBaby:1b,
id:"minecraft:chicken"
}]}
```

生成位置研究。好像不能随机生成，会被做成刷怪塔。

```
execute at @p[limit=1, sort=random, distance=..15] unless entity @e[tag=the_head_monster_7, distance=..100] run summon minecraft:pillager ~1 ~random ~1 {CustomName:"[{\"text\":\"打上花火\"}]",Size: 0, Tags:["the_head_monster_7_horse"],CustomNameVisible:1b,PersistenceRequired:1b,
Motion: [-0.3d, 2d, -0.2d]}
```

## 头活动8 - 多合一水陆海龟头怪

快速的水陆两用的怪物

放：Shizuku_Eai SaberArtoria Suadade_4049 MXiPhone
还有很多位置

30海豚恩惠，29潮涌能量，10生命恢复，8跳跃提升，24发光，18虚弱，4挖掘疲劳？ 12抗火 19 中毒(不致死) 20凋零

```
execute at @e[limit=1, sort=random, distance=..30] unless entity @e[tag=the_head_monster_8, distance=..100] run summon minecraft:phantom ~0 90 ~-32 {CustomName:"[{\"text\":\"头怪8\"}]",Size: 0, Tags:["the_head_monster_8"],CustomNameVisible:1b,PersistenceRequired:1b,
Motion: [1d, 0.1d, 1d],
ActiveEffects:[{Id:24,Amplifier:1,Duration:199999980}, {Id: 20, Amplifier: 5, Duration: 10000}],
Attributes:[{Name:"generic.movement_speed",Base:5}],
carriedBlockState:{Name:"minecraft:jack_o_lantern"},
Health: 12,
Passengers:[
{id:"turtle", Tags:["the_head_monster_8"], 
    ActiveEffects:[{Id:24,Amplifier:1,Duration:199999980}, {id: 28, Amplifier: 1, Duration: 19999998}, {id: 25, Amplifier: 1, Duration: 10000}, {Id:8,Amplifier:10,Duration:19999998}],IsBaby: 1b,
    Attributes:[{Name:"generic.movement_speed",Base:2}],
    Health: 12,
    Passengers: [{id:"zombie", Tags:["the_head_monster_8"], 
        IsBaby: 1b,
        ActiveEffects:[{Id:24,Amplifier:1,Duration:199999980}, {Id:12, Amplifier: 4, Duration: 10000}, {Id:8,Amplifier:5,Duration:10000}, {Id: 28, Amplifier: 1, Duration: 10000}],
        Attributes: [{Name:"generic.follow_range",Base:500}, {Name:"generic.movement_speed",Base:0.5}],
    },
    {id:"drowned", Tags:["the_head_monster_8"], 
        DrownedConversionTime: -1, IsBaby: 0b,
        HandItems: [{id:"minecraft:trident", Count: 1}],
        ActiveEffects:[{Id:24,Amplifier:1,Duration:199999980},{Id:8,Amplifier:10,Duration:19999998}],
        Attributes: [{Name:"generic.follow_range",Base:500}]
    },
    {id:"skeleton", Tags:["the_head_monster_8"],
        HandItems: [{id:"bow", Count: 1}],
        ActiveEffects:[{Id:24,Amplifier:1,Duration:199999980},{Id:8,Amplifier:10,Duration:19999998}],
    },
    {id:"guardian", Tags:["the_head_monster_8"],
        ActiveEffects:[{Id:24,Amplifier:1,Duration:199999980},{Id:8,Amplifier:10,Duration:19999998}],
        ArmorItems:[
            {},
            {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"CharAznable0079"}},
            {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"MXiPhone"}},
            {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"SaberArtoria"}}
            ],
        HandItems:[
            {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"Shizuku_Eai"}},
            {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"Suadade_4049"}},
        ],
        HandDropChances:[0.8f,0.8f],ArmorDropChances:[0.8f,0.8f,0.8f,0.8f],
    }],
}
]
}
```

叠叠乐尝试（试过了，只有最上面和最下面会动，中间的不会动，不太好）

```
execute at @p[limit=1, sort=random, distance=..30] unless entity @e[tag=the_head_monster_8, distance=..100] run summon minecraft:phantom ~0 ~5 ~0 {CustomName:"[{\"text\":\"头怪8\"}]",Size: 0, Tags:["the_head_monster_8"],CustomNameVisible:1b,PersistenceRequired:1b,
ActiveEffects:[{Id:24,Amplifier:1,Duration:199999980}, {Id: 20, Amplifier: 5, Duration: 10000}],
Attributes:[{Name:"generic.movement_speed",Base:5}],
carriedBlockState:{Name:"minecraft:jack_o_lantern"},
Health: 8,
ArmorItems:[
    {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"sq831quq"}},
    {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"bookonetwo"}},
    {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"Tree_Ocean"}},
    {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"Maqq_"}}
    ],
HandItems:[
    {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"oriannalulu"}},
    {id:"minecraft:player_head",Count:1b,tag:{SkullOwner:"nonesazen"}},
],
HandDropChances:[0.2f,0.2f],ArmorDropChances:[0.2f,0.2f,0.2f,0.2f],
Passengers:[
{id:"turtle", Tags:["the_head_monster_8"], 
    ActiveEffects:[{Id:24,Amplifier:1,Duration:199999980}, {id: 28, Amplifier: 1, Duration: 199999980}, {id: 25, Amplifier: 1, Duration: 10000}, {Id:8,Amplifier:10,Duration:10000}],IsBaby: 1b,
    Attributes:[{Name:"generic.movement_speed",Base:2}],
    HandItems: [{id:"minecraft:trident", Count: 1}],
    Passengers: [{id:"zombie", Tags:["the_head_monster_8"], 
        IsBaby: 1b,
        HandItems: [{id:"minecraft:trident", Count: 1}],
        ActiveEffects:[{Id:24,Amplifier:1,Duration:199999980}, {Id:12, Amplifier: 4, Duration: 10000}, {Id:8,Amplifier:10,Duration:10000}],
        Attributes: [{Name:"generic.follow_range",Base:500}, {Name:"generic.movement_speed",Base:5}],
        Passengers: [
            {id:"drowned", Tags:["the_head_monster_8"], 
                DrownedConversionTime: -1, IsBaby: 0b,
                HandItems: [{id:"minecraft:trident", Count: 1}],
                ActiveEffects:[{Id:24,Amplifier:1,Duration:199999980}],
                Attributes: [{Name:"generic.follow_range",Base:500}],
                Passengers: [
                    {id:"skeleton", Tags:["the_head_monster_8"],
                        HandItems: [{id:"bow", Count: 1}],
                        ActiveEffects:[{Id:24,Amplifier:1,Duration:199999980}],
                        Passengers: [
                            {id:"zombie", Tags:["the_head_monster_8"], 
                            IsBaby: 1b,
                            HandItems: [{id:"minecraft:trident", Count: 1}],
                            ActiveEffects:[{Id:24,Amplifier:1,Duration:199999980}, {Id:12, Amplifier: 4, Duration: 10000}, {Id:8,Amplifier:10,Duration:10000}],
                            Attributes: [{Name:"generic.follow_range",Base:500}, {Name:"generic.movement_speed",Base:5}]}
                        ],
                    },
                ],
            }
        ]
    },
    {id:"guardian", Tags:["the_head_monster_8"],
        HandItems: [{id:"bow", Count: 1}],
        ActiveEffects:[{Id:24,Amplifier:1,Duration:199999980}],
    }],
}
]
}
```